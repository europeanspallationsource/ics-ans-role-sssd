ics-ans-role-sssd
=================

Ansible role to install SSSD (System Security Services Daemon).

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

Windows AD
```yaml
sssd_disabled: False
sssd_ldap_domain: esss.lu.se
sssd_ldap_uri: ldap://esss.lu.se
sssd_ldap_search_base: dc=esss,dc=lu,dc=se
sssd_ldap_default_bind_dn: cn=ldapreadonly,cn=Users,dc=esss,dc=lu,dc=se
sssd_ldap_default_authtok: password
```

OpenLdap
```yaml
sssd_ldap_default_authtok: SomePass
sssd_ldap_default_bind_dn: "uid=ABot,ou=Service accounts,dc=esss,dc=lu,dc=se"
sssd_ldap_uri: ldap://ldap.ics.esss.lu.se
sssd_ldap_schema: rfc2307
sssd_ldap_id_mapping: "False"
sssd_ldap_referrals: "True"
```

Set `sssd_disabled` to True to disable SSSD.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-sssd
```

License
-------

BSD 2-clause
